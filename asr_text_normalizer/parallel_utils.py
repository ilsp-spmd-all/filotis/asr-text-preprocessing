import math
import sys
from typing import Any, Callable, List

try:
    from joblib import Parallel, delayed
    from tqdm import tqdm
except ImportError:
    print(
        "You should install joblib and tqdm for parallel processing. Try using --nj=1 or installing dependencies.",
        file=sys.stderr,
    )
    raise


class ProgressParallel(Parallel):
    def __init__(self, use_tqdm=True, total=None, *args, **kwargs):
        self._use_tqdm = use_tqdm
        self._total = total
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        with tqdm(disable=not self._use_tqdm, total=self._total) as self._pbar:
            return Parallel.__call__(self, *args, **kwargs)

    def print_progress(self):
        if self._total is None:
            self._pbar.total = self.n_dispatched_tasks
        self._pbar.n = self.n_completed_tasks
        self._pbar.refresh()


def chunker(iterable, total_length, chunksize):
    return (
        iterable[pos : pos + chunksize] for pos in range(0, total_length, chunksize)
    )


def flatten(list_of_lists):
    "Flatten a list of lists to a combined list"
    return [item for sublist in list_of_lists for item in sublist]


def process_parallel(
    process_fn: Callable[..., Any],
    data: List,
    batch_size: int = 200,
    nj: int = 2,
    **kwargs,
) -> List:
    executor = ProgressParallel(
        n_jobs=nj,
        backend="multiprocessing",
        prefer="processes",
        total=math.ceil(len(data) / batch_size),
    )
    do = delayed(process_fn)
    tasks = (
        do(chunk, **kwargs) for chunk in chunker(data, len(data), chunksize=batch_size)
    )
    result = executor(tasks)
    result = flatten(result)
    return result
