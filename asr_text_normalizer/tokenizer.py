import argparse
import hashlib
import sys
from typing import Optional

try:
    import spacy
    from spacy.cli.download import download
except ImportError:
    sys.exit(
        "You should install 'spacy' to be able to use the tokenizer: pip install spacy==3.4.4"
    )
from tqdm import tqdm

import asr_text_normalizer.config as cfg

SPACY_MODELS = {
    "gr": "el_core_news_sm",
    "el": "el_core_news_sm",
    "en": "en_core_web_sm",
}

NLP = None


def configure_spacy_model(lang):
    """Load and configure spacy"""
    global NLP
    if lang not in SPACY_MODELS:
        sys.exit(
            f"The language you provided {lang} is not supported. Supported languages: "
            f"{list(SPACY_MODELS.keys())}"
        )
    try:
        nlp = spacy.load(
            SPACY_MODELS[lang],
            disable=[
                "tagger",
                "parser",
                "ner",
                "tok2vec",
                "attribute_ruler",
                "lemmatizer",
            ],
        )
    except OSError:
        # the model isn't available, download it and re-try
        download(SPACY_MODELS[lang])
        nlp = spacy.load(
            SPACY_MODELS[lang],
            disable=[
                "tagger",
                "parser",
                "ner",
                "tok2vec",
                "attribute_ruler",
                "lemmatizer",
            ],
        )

    nlp.add_pipe("sentencizer")

    NLP = nlp
    # return nlp


def keep_token(t, keep_alphanumeric=False):
    if keep_alphanumeric:
        return t.is_alpha or t.is_digit
    return True


def deduplicate(lines):
    seen = set()
    for line in lines:
        if line is None:
            continue
        line_hash = hashlib.md5(line.encode()).digest()
        if line_hash not in seen:
            seen.add(line_hash)
            yield line


def tokenized_sentence(doc, config):
    # Tokenizer removes anything that is not alphanumeric
    # If you need punctuation remove checks.
    doc = [t for t in doc]
    if config.min_sentence_length > 0:
        if len(doc) < config.min_sentence_length:
            return None
    sentence = " ".join(
        [
            t.text
            for t in doc
            if keep_token(t, keep_alphanumeric=config.keep_alphanumeric)
        ]
    )
    if config.remove_unfinished_lines:
        if sentence.endswith("..."):
            return None
    # Comment out if you need to preserve accents
    return sentence


def sentence_split(doc):
    sents = []
    for s in doc.sents:
        current_sentence = [t for t in s]
        if len(sents) > 0:
            # Fix some spacy sentence splitting errors by joining sentences if they don't end in a period
            if len(sents[-1]) and sents[-1][-1].text != ".":
                sents[-1] = [t for t in sents[-1]] + current_sentence
                continue
        sents.append(current_sentence)
    return sents


def process_line(doc, config):
    sents = sentence_split(doc) if config.split_sentences else [doc]
    processed_sents = [
        tokenized_sentence(sent, config) for sent in sents if sent is not None
    ]
    return processed_sents


def process_batch(lines, config=None, progress=False, pipe_batch_size=10):
    processed_sents = []
    if NLP is None:
        raise ValueError(
            "Should initialize global NLP variable before running. Run: configure_spacy_model(lang)"
        )
    piped = NLP.pipe(  # Ugly use of global variable, but for some reason parallel processing is much faster this way
        lines,
        batch_size=pipe_batch_size,
    )
    if progress:
        piped = tqdm(piped, total=len(lines))
    for doc in piped:
        processed_sents += process_line(doc, config)
    return processed_sents


def tokenize_txt(
    txt_path: str,
    encoding: str,
    config: cfg.TokenizeConfig,
    out_file: Optional[str] = None,
    nj: int = 1,
    batch_size: int = 100,
):
    configure_spacy_model(config.lang)
    with open(txt_path, "r", encoding=encoding, errors="ignore") as file:
        lines = [ln.strip() for ln in file]
    if nj == 1:
        # single process
        processed = process_batch(lines, config=config, progress=True)
    else:
        import asr_text_normalizer.parallel_utils as parallel

        processed = parallel.process_parallel(
            process_batch,
            lines,
            batch_size=batch_size,
            nj=nj,
            config=config,
        )
    if config.deduplicate:
        processed = deduplicate(processed)
    if out_file is not None:
        with open(out_file, "w", encoding=encoding) as fd:
            for ln in processed:
                fd.write(f"{ln}\n")
    else:
        for ln in processed:
            print(f"{ln}")

    return processed


def tokenize_string(txt, lang="en", keep_alphanumeric=True, split_sentences=True):
    # Only valid for short strings. Normally, you would  tokenize a whole file
    configure_spacy_model(lang)
    conf = cfg.TokenizeConfig(
        lang=lang, keep_alphanumeric=keep_alphanumeric, split_sentences=split_sentences
    )
    doc = NLP(txt)
    sentences = process_line(doc, conf)
    return sentences


def parse_args():
    parser = argparse.ArgumentParser(
        description="Script to tokenize a text file for ASR training and testing",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--txt-path",
        "--text",
        "-t",
        help="Path to text text file",
        type=str,
        required=True,
    )

    parser.add_argument(
        "--out-file", help="Path to output text file", type=str, default=None
    )
    parser.add_argument(
        "--encoding",
        help="Text file encoding.",
        default="utf-8",
    )

    parser = cfg.TokenizeConfig.add_argparse_args(parser)

    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    config = cfg.TokenizeConfig.from_args(args)
    _ = tokenize_txt(
        txt_path=args.txt_path,
        encoding=args.encoding,
        config=config,
        nj=args.nj,
        out_file=args.out_file,
    )


if __name__ == "__main__":
    main()
