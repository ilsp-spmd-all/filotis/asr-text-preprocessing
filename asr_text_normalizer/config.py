import abc
import argparse
import warnings
from dataclasses import dataclass, field, fields
from typing import Optional


class NegateAction(argparse.Action):
    def __call__(self, parser, ns, values, option):
        setattr(ns, self.dest, option[2:4] != "no")


def add_argument_if_not_added(parser, *args, **kwargs):
    try:
        parser.add_argument(*args, **kwargs)
    except argparse.ArgumentError:
        pass


@dataclass
class Configuration(abc.ABC):
    lang: Optional[str] = field(default=None)
    nj: int = field(default=1)

    @classmethod
    def from_args(cls, args: argparse.Namespace):
        params = vars(args)
        # Keep only relevant cmd line args for initialization
        class_fields = {f.name for f in fields(cls)}
        return cls(**{k: v for k, v in params.items() if k in class_fields})

    @staticmethod
    def add_argparse_args(parser: argparse.ArgumentParser):
        add_argument_if_not_added(
            parser,
            "--lang",
            help="Language code; it is mainly needed for number/date conversion during "
            "normalization, or for the SpaCy model during tokenization. For instance, use "
            "'pk' for Pomak or 'el' for Greek. If left empty, the dates and numbers will "
            "not be converted.",
        )

        add_argument_if_not_added(
            parser,
            "--nj",
            type=int,
            help="Number of jobs to use. Single thread by default",
            default=1,
        )
        return parser


@dataclass
class TokenizeConfig(Configuration):
    keep_alphanumeric: bool = field(default=False)
    split_sentences: bool = field(default=False)
    remove_unfinished_lines: bool = field(default=False)
    min_sentence_length: int = field(default=-1)
    deduplicate: bool = field(default=False)

    @staticmethod
    def add_argparse_args(parser: argparse.ArgumentParser):
        parser = super(TokenizeConfig, TokenizeConfig).add_argparse_args(parser)
        parser.add_argument(
            "--keep-alphanumeric",
            "--no-keep-alphanumeric",
            dest="keep_alphanumeric",
            default=False,
            nargs=0,
            action=NegateAction,
            help="Keep only alphanumeric characters",
        )
        parser.add_argument(
            "--deduplicate",
            "--no-deduplicate",
            dest="deduplicate",
            default=False,
            nargs=0,
            action=NegateAction,
            help="Deduplicate output lines",
        )
        parser.add_argument(
            "--remove-unfinished-lines",
            "--no-remove-unfinished-lines",
            dest="remove_unfinished_lines",
            default=False,
            nargs=0,
            action=NegateAction,
            help="Remove lines that end with '...'. Valid for CCNet / Common Crawl style datasets.",
        )
        parser.add_argument(
            "--min-sentence-length",
            dest="min_sentence_length",
            default=-1,
            type=int,
            help="Filter out sentences that are smaller than min_sentence_length. "
            "Useful for corpus creation.",
        )

        return parser


@dataclass
class NormalizeConfig(Configuration):
    replace_characters: bool = field(default=False)
    do_lowercase: bool = field(default=True)
    remove_accents: bool = field(default=False)
    lexicalize_dates_and_numbers: bool = field(default=True)
    remove_punctuation: bool = field(default=True)

    def __post_init__(self):
        if not self.lang:
            # Throw warning and override if lang is not provided
            warnings.warn(
                "You must provide a language code to convert dates and numbers into their literal equivalent. "
                "Continuing without the lexicalization feature."
            )
            self.lexicalize_dates_and_numbers = False

    @staticmethod
    def add_argparse_args(parser: argparse.ArgumentParser):
        parser = super(NormalizeConfig, NormalizeConfig).add_argparse_args(parser)
        parser.add_argument(
            "--lowercase",
            "--no-lowercase",
            dest="do_lowercase",
            default=True,
            nargs=0,
            action=NegateAction,
            help="Convert to lowercase",
        )

        parser.add_argument(
            "--remove-accents",
            "--no-remove-accents",
            dest="remove_accents",
            default=False,
            nargs=0,
            action=NegateAction,
            help="Remove accents from characters",
        )

        parser.add_argument(
            "--remove-punctuation",
            "--no-remove-punctuation",
            dest="remove_punctuation",
            default=True,
            nargs=0,
            action=NegateAction,
            help="Remove punctuation from sentences",
        )
        parser.add_argument(
            "--lexicalize-dates-and-numbers",
            "--no-lexicalize-dates-and-numbers",
            dest="lexicalize_dates_and_numbers",
            default=True,
            nargs=0,
            action=NegateAction,
            help="Convert dates and numbers to lexical representations",
        )

        return parser
