"""
Text normalization for speech technologies
"""
import argparse
import os
import re
import unicodedata
from pathlib import Path
from typing import Optional

from num2word_greek import numbers2words as num2words_gr

import asr_text_normalizer.config as cfg
from asr_text_normalizer.num2words import num2words

DONT_STRIP_TOKENS = (
    "<unk>",
    "[UNK]",
    "<pad>",
    "[PAD]",
    "<s>",
    "</s>",
    "[SPN]",
    "<spoken-noise>",
)

_RE_MULTIPLE_WHITESPACE = re.compile(r"\s+")
# To add a new language, make sure you add a dict with the month names
MONTHS = {
    "pk": {
        1: "Ianuários",
        2: "Fevruários",
        3: "Márta",
        4: "Aprílios",
        5: "Májs",
        6: "Iúnios",
        7: "Iúlios",
        8: "Ávgustos",
        9: "Septémvrios",
        10: "Októvrios",
        11: "Kásym",
        12: "Dekémvrios",
    },
    "el": {
        1: "Ιανουαρίου",
        2: "Φεβρουαρίου",
        3: "Μαρτίου",
        4: "Απριλίου",
        5: "Μαίου",
        6: "Ιουνίου",
        7: "Ιουλίου",
        8: "Αυγούστου",
        9: "Σεπτεμβρίου",
        10: "Οκτωβρίου",
        11: "Νοεμβρίου",
        12: "Δεκεμβρίου",
    },
    "en": {
        1: "of January",
        2: "of February",
        3: "of March",
        4: "of April",
        5: "of May",
        6: "of June",
        7: "of July",
        8: "of August",
        9: "of September",
        10: "of October",
        11: "of November",
        12: "of December",
    },
}
# Greek has two language codes: el, gr. Make sure
# the months work for either code.
MONTHS["gr"] = MONTHS["el"]


def number_conversion(number, lang, number_type="cardinal"):
    """num2words function that throws a warning in case the number-to-language rules
    haven't been defined for this language code; in that case, the original
    number will be returned
    @number: the number integer
    @lang: the language code (2 characters, e.g., 'pk' for Pomak)
    @number_type: by default, the conversion is to a cardinal number. Specify whether you
                  need an ordinal one."""
    try:
        if lang in ["el", "gr"]:
            return num2words_gr.convert_sentence(str(number))
        # for all other languages, use the num2words package
        return num2words(number, lang=lang, to=number_type)
    except NotImplementedError:
        print(
            "num2words is not available for this language code. Please make sure you create a "
            f"lang_{lang.upper()}.py file with all the language rules under the num2words/ folder."
        )
        return str(number)


def date2words(sentence, lang, return_separator=False):
    """
    regular expression that matches a date in the following formats:
    dd-mm-yyyy, dd-mm-yy, dd/mm/yyyy, dd/mm/yy. Single digits for day and month are also captured.
    @sentence: a sentence string, e.g.: "When? On 21/05/11, 21-09-1991, 12/05/2010 or 12-4-33?"
    @lang: the language code. E.g., "pk" for Pomak
    @return_separator: Whether to return the separator ("/", "-") or to simply leave a space
    """
    # if there is a match, it returns tuples of the seperator together with the date, e.g.:
    # [('21', '/', '05', '/', '11'), ('21', '-', '09', '-', '1991'),
    # ('12', '/', '05', '/', '2010'), ('12', '-', '4', '-', '33')]
    try:
        match = re.findall(
            r"(\d{1}(?:\d{1})?)(\-|\/)(\d{1}(?:\d{1})?)(\-|\/)(\d{2}(?:\d{2})?)",
            sentence,
        )
        if match:
            for date in match:
                # for each match, replace the month digit with its literal
                day = number_conversion(int(date[0]), lang=lang, number_type="ordinal")
                month = MONTHS[lang][int(date[2])]
                year = number_conversion(int(date[4]), lang=lang, number_type="year")
                separator = date[1] if return_separator else " "
                sentence = sentence.replace(
                    f"{''.join(date)}", f"{day}{separator}{month}{separator}{year}"
                )
        return sentence
    except:
        return sentence


def replace_num(match, lang):
    """
    Receives a regex digit match and replaces that digit with its literal
    """
    return number_conversion(int(match.group()), lang=lang)


# you may want to replace pomak characters that don't exist in current models,
# for instance: {"ņ": "n", "ü": "u", "ǘ": "u"}
CHARS = {}


def replace_chars(sentence):
    """
    replaces certain accented characters (from dict CHARS) in a string
    @sentence: sentence string
    """
    res = ""
    for character in sentence:
        new = CHARS[character] if character in CHARS else character
        res += new

    return res


def normalize_batch(lines_batch, config=None):
    batched_results = [normalize_line(ln, config=config) for ln in lines_batch]
    # return the (non-empty) results
    return list(filter(None, batched_results))


def normalize_txt(
    txt_path: str,
    config: cfg.NormalizeConfig,
    out_file: Optional[str] = None,
    nj: int = 1,
    batch_size: int = 200,
    encoding="utf-8",
):
    """
    Load txt as one line string and normalize it
    @txt_path: path to text file, e.g., '/data/scratch/filotis/studio_xanthis_16kHz/R_02_9G75fk.txt'
    @encoding: file encoding. Usually it's utf-8.
    @config: A NormalizeConfig instance containing the relevant configuration
    @nj: Number of jobs
    @batch_size: lines per job
    """

    with open(txt_path, "r", encoding=encoding) as file:
        lines = [ln.strip() for ln in file]
    if nj == 1:
        # single process
        processed = normalize_batch(lines, config=config)
    else:
        import asr_text_normalizer.parallel_utils as parallel

        processed = parallel.process_parallel(
            normalize_batch, lines, batch_size=batch_size, nj=nj, config=config
        )
    if out_file is not None:
        with open(out_file, "w", encoding=encoding) as fhandler:
            for line in processed:
                fhandler.write(f"{line}\n")

    return processed


def normalize_dir(
    txt_dir: str,
    output_dir: str,
    lang: str,
    encoding="utf-8",
    nj=1,
    replace_characters=False,
    do_lowercase=True,
    remove_accents=False,
    lexicalize_dates_and_numbers=True,
    remove_punctuation=True,
):
    """Helper script that normalizes each file in a folder
    @txt_dir: path to a folder containing text files
    @output_dir: path to output folder
    @lang: language code (e.g., "en" for English). It is optional and required only
           for number lexicalization.
    @encoding: file encoding. Usually it's utf-8.
    @nj: Number of jobs
    @do_lowercase: whether to lowecase all characters
    @remove_accents: whether to replace accents (e.g., á -> a)
    @lexicalize_dates_and_numbers: whether to convert dates and numbers
                                   into their word equivalent
    @remove_punctuation: whether to remove all special characters"""
    os.makedirs(output_dir, exist_ok=True)
    conf = cfg.NormalizeConfig(
        lang=lang,
        replace_characters=replace_characters,
        do_lowercase=do_lowercase,
        remove_accents=remove_accents,
        lexicalize_dates_and_numbers=lexicalize_dates_and_numbers,
        remove_punctuation=remove_punctuation,
    )
    for fname in Path(txt_dir).rglob("*.txt"):
        normalize_txt(
            txt_path=fname,
            encoding=encoding,
            config=conf,
            nj=nj,
            out_file=os.path.join(output_dir, os.path.basename(fname)),
        )


def strip_accents(sentence):
    """Replaces accented characters with their non-accented equivalent. E.g., á becomes a"""
    return "".join(
        c
        for c in unicodedata.normalize("NFD", sentence)
        if unicodedata.category(c) != "Mn"
    )


def numbers2words(new_txt, lang_code):
    """Lexicalizes numbers by calling the corresponding package
    (num2words_gr for Greek, num2words for other languages)"""
    try:
        if lang_code in ["gr", "el"]:
            return num2words_gr.convert_sentence(new_txt)
        else:
            return re.sub(r"\d+", lambda x: replace_num(x, lang=lang_code), new_txt)
    except:
        return new_txt


def strip_punctuation_token(tok):
    """Strips tokens and replaces dashes and forward slashes with space,
    to better handle dates"""
    tok = "".join(c for c in tok if c.isalnum() or c.isspace() or c == "/" or c == "-")
    tok = tok.replace("/", " ")
    tok = tok.replace("-", " ")
    return tok


def strip_punctuation(new_txt, dont_strip_tokens=DONT_STRIP_TOKENS):
    """Strips all punctuation marks"""
    # chars_to_ignore = "[\[\,\?\.\!\-\;\:\/\"\“\„\%\”\�\–'\`\«\»\(\)\—\’\…\³'̌\•\]]"
    # new_txt = re.sub(chars_to_ignore, " ", new_txt)
    tokens = new_txt.split()
    tokens = [strip_punctuation_token(t) for t in tokens if t not in dont_strip_tokens]
    new_txt = " ".join(tokens)
    return new_txt


def handle_whitespaces(new_txt):
    """Remove any remaining leading and trailing white spaces, plus multiple spaces"""
    new_txt = new_txt.lstrip().rstrip()

    # Remove multiple spaces,
    # e.g. 'The     quick brown    fox' -> 'The quick brown fox'
    # new_txt = re.sub(" +", " ", new_txt)
    # A bit more general... Handles "  ", "\t\t", "\t " etc.
    new_txt = _RE_MULTIPLE_WHITESPACE.sub(" ", new_txt).strip()

    return new_txt


def normalize_line(txt_str: str, config: cfg.NormalizeConfig):
    """
    Normalize a string by (optionally):
    - replacing dates and numbers with their literal
    - removing special characters and punctuation marks
    - removing accents
    - lowering the text
    - striping trailing white space
    @txt_str: a text string
    @config: a NormalizeConfig instance with relevant configuration
    """
    new_txt = txt_str
    # Date and number conversions are language-specific.
    # If the lang code is left empty, these steps are skipped.
    if config.lexicalize_dates_and_numbers:
        assert (
            config.lang is not None
        ), "You must provide a language code to replace dates"
        # Find and replace all dates
        new_txt = date2words(sentence=new_txt, lang=config.lang)
        new_txt = numbers2words(new_txt, config.lang)

    if config.remove_punctuation:
        # Ignore special characters
        new_txt = strip_punctuation(new_txt)

    if config.replace_characters:
        new_txt = replace_chars(new_txt)

    if config.do_lowercase:
        # don't lowercase when aligning text and audio, for instance
        new_txt = new_txt.lower()

    if config.remove_accents:
        new_txt = strip_accents(new_txt)

    # Remove any remaining leading and trailing white spaces + multiple whitespaces
    new_txt = handle_whitespaces(new_txt)
    return new_txt


def normalize_string(
    txt: str,
    lang="en",
    replace_characters=False,
    do_lowercase=True,
    remove_accents=False,
    lexicalize_dates_and_numbers=True,
    remove_punctuation=True,
):
    """Creates the configuration and calls normalize_line"""
    conf = cfg.NormalizeConfig(
        lang=lang,
        replace_characters=replace_characters,
        do_lowercase=do_lowercase,
        remove_accents=remove_accents,
        lexicalize_dates_and_numbers=lexicalize_dates_and_numbers,
        remove_punctuation=remove_punctuation,
    )
    return normalize_line(txt, conf)


def parse_args():
    """Parse the command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Script to preprocess a text for ASR training and testing",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--txt-path",
        "--text",
        "-t",
        help="Path to a single text file or to a directory containing multiple text files",
        type=str,
        required=True,
    )

    parser.add_argument(
        "--out-file",
        help="Path to output text file (for a single input file) or to an output directory",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--encoding",
        help="Text file encoding",
        default="utf-8",
    )

    parser = cfg.NormalizeConfig.add_argparse_args(parser)

    args = parser.parse_args()
    return args


def main():
    """Main function that parses the CL arguments and
    normalizes a single text, or all texts in a directory"""
    args = parse_args()
    config = cfg.NormalizeConfig.from_args(args)

    if os.path.isfile(args.txt_path):
        processed_text = normalize_txt(
            txt_path=args.txt_path,
            encoding=args.encoding,
            config=config,
            nj=args.nj,
            out_file=args.out_file,
        )
        print(processed_text)
    elif os.path.isdir(args.txt_path):
        # create the output directory
        os.makedirs(args.out_file, exist_ok=True)
        # process all files in the input directory
        for fname in Path(args.txt_path).rglob("*.txt"):
            normalize_txt(
                txt_path=fname,
                encoding=args.encoding,
                config=config,
                nj=args.nj,
                out_file=os.path.join(args.out_file, os.path.basename(fname)),
            )


if __name__ == "__main__":
    main()
