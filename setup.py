from setuptools import setup

setup(
    name="asr-text-normalizer",
    version="0.1.0",
    description="Simple text pre-processing for ASR. It includes date and number conversion into words",
    url="https://gitlab.com/ilsp-spmd-all/filotis/asr-text-preprocessing/",
    author="Chara Tsoukala - Giorgos Paraskevopoulos - Kosmas Kritsis",
    license_files=("MIT_LICENSE",),
    packages=["asr_text_normalizer", "asr_text_normalizer/num2words"],
    install_requires=[
        "joblib==1.1.1",
        "Numbers2Words-Greek==0.0.2a0",
        "tqdm",
        "spacy==3.4.4",
    ],
    setup_requires=["flake8", "wheel"],
),
