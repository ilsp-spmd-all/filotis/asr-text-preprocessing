# ASR text preprocessing

Text normalization as a pre-processing step for speech technologies (Automatic Speech Recognition [ASR], Text to Speech [TTS]).

The (optional) pre-processing steps are the following:
- Dates are converted into words. Numbers are converted into words using the `num2words` (*) and `Numbers2Words-Greek` (**) package
- All punctuation marks are removed
- (Optionally) some characters are replaced; see step 3 below
- All characters are lower-cased
- (Optionally) the accents are removed
- Multiple spaces and any remaining leading and trailing white spaces are removed

(*) The `num2words` folder is a fork of `https://github.com/savoirfairelinux/num2words`, containing an additional file for the Pomak (`pk`) language that wasn't supported (`lang_PK.py`)

(**) Greek was not a supported language; we used the `Numbers2Words-Greek` package, which is part of the installation requirements.

# How to create a dataset in other languages:

Some pre-processing steps that have to do with number/date processing are language-specific. If your language is not supported language, you will need to edit `asr_text_normalizer/normalizer.py`:
1. If you are interested in date conversion, add your language code and the corresponding months to the `MONTHS` dict (currently on L27)
2. If you are interested in number conversion, make sure your language code exists under the `num2words` folder (e.g., `lang_EN.py` for English.). If it doesn't exist, you will need to create a new file and edit it according to the language rules. If the text normalizer script doesn't find the corresponding file, it will throw a warning letting you know that the numbers will *not* be converted.
3. If you want to simplify an alphabet (e.g., `ü -> u`), edit the `CHARS` dict.

Currently, all punctuation marks and special characters are being removed during pre-processing. If you want to keep some tokens (including punctuation marks), add them to the following tuple:

```
DONT_STRIP_TOKENS = (
    "<unk>",
    "[UNK]",
    "<pad>",
    "[PAD]",
    "<s>",
    "</s>",
    "[SPN]",
    "<spoken-noise>",
)
```

# How to install

The project is pip-installable. For now it is a private repository, so you can `cd` to the root project folder and run:

`pip install .`

# How to normalize a text file via the command line

`python asr_text_normalizer/normalizer.py -h`

```
usage: normalizer.py [-h] --txt-path TXT_PATH [--out-file OUT_FILE] [--encoding ENCODING]
                     [--lang LANG] [--nj NJ] [--lowercase] [--remove-accents]
                     [--remove-punctuation] [--lexicalize-dates-and-numbers]

Script to preprocess a text for ASR training and testing

optional arguments:
  -h, --help            show this help message and exit
   --txt-path TXT_PATH, --text TXT_PATH, -t TXT_PATH
                        Path to a single text file or to a directory containing multiple text files (default: None)
  --out-file OUT_FILE   Path to output text file (for a single input file) or to an output directory (default: None)
  --encoding ENCODING   Text file encoding (default: utf-8)
  --lang LANG           Language code; it is mainly needed for number/date conversion during normalization, or for the SpaCy model during tokenization. For instance, use 'pk' for Pomak or 'el' for Greek. If left empty, the dates and
                        numbers will not be converted. (default: None)
  --nj NJ               Number of jobs to use. Single thread by default (default: 1)
  --lowercase, --no-lowercase
                        Convert to lowercase (default: True)
  --remove-accents, --no-remove-accents
                        Remove accents from characters (default: False)
  --remove-punctuation, --no-remove-punctuation
                        Remove punctuation from sentences (default: True)
  --lexicalize-dates-and-numbers, --no-lexicalize-dates-and-numbers
                        Convert dates and numbers to lexical representations (default: True)
```

For instance, you can try the provided example (`examples/en_example.txt`) in English. The original text says: `A "lucky prime" is a lucky number that is prime. The 5 first ones are: 3, 7, 13, 31, 37. Retrieved from Wikipedia on 16/12/2022.`

If you process this text via:

`python asr_text_normalizer/normalizer.py --text examples/en_example.txt --lang en`

It should output: `a lucky prime is a lucky number that is prime the five first ones are three seven thirteen thirty one thirty seven retrieved from wikipedia on sixteenth of december twenty twenty two`.

Whereas if you don't provide the language code, the date and number conversions will be skipped and the output will be:

`a lucky prime is a lucky number that is prime the 5 first ones are 3 7 13 31 37 retrieved from wikipedia on 16 12 2022`

You will get the same output if you use the `--no-lexicalize-dates-and-numbers` option.

To retain the punctuation marks, use the `--no-remove-punctuation` flag:

`a "lucky prime" is a lucky number that is prime. the five first ones are: three, seven, thirteen, thirty-one, thirty-seven. retrieved from wikipedia on sixteenth of december twenty twenty-two.`

If you want to remove accents, for instance in Greek, use the `--remove-accents` flag:

`python asr_text_normalizer/normalizer.py --text examples/el_example.txt --lang el --remove-accents` will output:

`ενας τυχερος πρωτος ειναι ενας τυχερος αριθμος που ειναι πρωτος οι πεντε πρωτοι ειναι τρια εφτα δεκατρια τριανταενα τριανταεφτα που ανακτηθηκαν απο τη wikipedia στις δεκαεξι δεκεμβριου δυο χιλιαδες εικοσιδυο`

# How to use as a package

After pip-installing the package, you can call the following functions:

1. `from asr_text_normalizer.normalizer import normalize_string`. You can then call `normalize_string('this is a text')`  with the following optional arguments:
    - lang: the 2-letter language code. The default is `en` for English and it is needed for date/number conversion. If the language code doesn't exist, you will get a warning and the numbers will not be converted.
    - remove_accents: whether to remove the accents (True by default)
    - lexicalize_dates_and_numbers: whether to convert dates and numbers (True by default),
    - remove_punctuation: whether to remove punctuation marks (True by default),  
    - replace_characters: whether to replace specific characters (specified in the CHARS dict). By default it's False.
    - do_lowercase: whether to lowercase all words (True by default)
2. `from asr_text_normalizer.normalizer import date2words`. The date2words function requires a language code. For instance, `date2words('12/12/86', lang='en')` will return `twelfth of December eighty-six`
3. `from asr_text_normalizer.normalizer import num2words`. This function is the [num2words](https://github.com/savoirfairelinux/num2words) fork that contains an additional entry for Pomak. If no language code is provided, the number will be converted into English. For instance, `num2words('21313')` returns `twenty-one thousand, three hundred and thirteen` whereas with `lang='pk'` it returns the Pomak equivalent: `jirmíbirbin üč jus onüč`
4. `from asr_text_normalizer.normalizer import number_conversion` it is a wrapper to num2words that includes the Greek package and catches the NotImplementedError in case the language code is unknown. For instance, for  `number_conversion('231342', lang='random')` you will get a warning plus the original number: `num2words is not available for this language code. Please make sure you create a lang_RANDOM.py file with all the language rules under the num2words/ folder. '231342'`
5. `normalize_dir` to normalize all files in a directory:

```
from asr_text_normalizer.normalizer import normalize_dir

parallel_jobs = 10
language_code = 'en'
normalize_dir(txt_dir=/path/to/text/files, output_dir='output', nj=parallel_jobs, lang=language_code)
```

# How to tokenize a file via the command line:

`python asr_text_normalizer/tokenizer.py -h`

```
usage: tokenizer.py [-h] --txt-path TXT_PATH [--out-file OUT_FILE]
                    [--encoding ENCODING] [--lang LANG] [--nj NJ]
                    [--keep-alphanumeric] [--deduplicate]
                    [--remove-unfinished-lines]
                    [--min-sentence-length MIN_SENTENCE_LENGTH]

Script to tokenize a text file for ASR training and testing

optional arguments:
  -h, --help            show this help message and exit
  --txt-path TXT_PATH, --text TXT_PATH, -t TXT_PATH
                        Path to text text file (default: None)
  --out-file OUT_FILE   Path to output text file (default: None)
  --encoding ENCODING   Text file encoding. (default: utf-8)
  --lang LANG           Language code; it is mainly needed for number/date
                        conversion during normalization, or for the SpaCy
                        model during tokenization. For instance, use 'pk' for
                        Pomak or 'el' for Greek. If left empty, the dates and
                        numbers will not be converted. (default: None)
  --nj NJ               Number of jobs to use. (default: 1)
  --keep-alphanumeric, --no-keep-alphanumeric
                        Keep only alphanumeric characters (default: False)
  --deduplicate, --no-deduplicate
                        Deduplicate output lines (default: False)
  --remove-unfinished-lines, --no-remove-unfinished-lines
                        Remove lines that end with '...'. Valid for CCNet /
                        Common Crawl style datasets. (default: False)
  --min-sentence-length MIN_SENTENCE_LENGTH
                        Filter out sentences that are smaller than
                        min_sentence_length. Useful for corpus creation.
                        (default: -1)
```

For instance, to tokenize the Greek text file, run:

`python asr_text_normalizer/tokenizer.py --txt examples/el_example.txt --lang el`

The tokenizer currently works only for Greek (`el`, `gr`) and English (`en`). If you want to use a different SpaCy model, add it to the `SPACY_MODELS` dict with the corresponding language code (currently on L17).

To tokenize a string inside a script, run:

```
from asr_text_normalizer.tokenizer import tokenize_string

tokenize_string('This is it!', lang='en', keep_alphanumeric=False)
```

which will output: `['This is it !']`
